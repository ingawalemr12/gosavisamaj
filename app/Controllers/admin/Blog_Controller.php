<?php

namespace App\Controllers\admin;
use App\Controllers\BaseController; 

class Blog_Controller extends BaseController
{
	public function index()
	{
		return view('admin/blog');
	}

	public function view()
	{
		return view('admin/blog_view');
	}
}

