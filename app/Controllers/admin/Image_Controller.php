<?php

namespace App\Controllers\admin;

use App\Controllers\BaseController; 
use App\Models\Image_Model;

class Image_Controller extends BaseController
{
	public function index()
	{
		return view('admin/imageupload');
	}

	public function imageStore()
	{
		$session = \Config\Services::session();
		$model = new Image_Model();

		$file = $this->request->getFile('image');
		if ($file->isValid() && $file->getSize() >0) 
		{
			$fileName = $file->getRandomName();
			$file->move('public/assets/images/adminPhotos/', $fileName);
		}

		$data = [
			'mobile' => $this->request->getPost('mobile'),
			'name' => $this->request->getPost('name'),
			'city' => $this->request->getPost('city'),
			'image' => $fileName, 
			'description' => $this->request->getPost('description'),
		];
		$model->save($data);

		$session->setFlashdata('success','Thanks, photo and record saved successfully');
		return redirect()->to('/admin');

	}
}

