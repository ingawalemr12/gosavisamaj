<?php

namespace App\Controllers\admin;
use App\Controllers\BaseController; 

class Admin_Controller extends BaseController
{
	public function index()
	{
		return view('admin/home');
	}

	public function imageUpload()
	{
		return view('admin/imageupload');
	}
}

