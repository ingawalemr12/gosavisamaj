<?php

namespace App\Controllers;

class UserController extends BaseController
{
	public function index()
	{
		return view('user/homePage');
	}

	public function contact()
	{
		$data = array();
		
		helper('form');
		if ($this->request->getMethod() == 'post') {
			//do validation
			$input = $this->validate([
				'name'=> 'trim|required',
				'email'=> 'trim|required|valid_email',
				'subject'=> 'trim|required|min_length[20]',	
				]);
			
			if($input == 'true')
			{
				//validation success
				$session = \Config\Services::session();
				$email = \Config\Services::email();
				$email->setFrom('ingawalemr12@gmail.com', 'Gosavi Community');
				//$email->setTo('one@example.com, two@example.com, three@example.com');
				$email->setTo('ingawalemr12@gmail.com');
				$email->setCC('ingawalemr12@yahoo.com');
				$email->setBCC('ingawalemr12@gmail.com');

				$name = $this->request->getPost('name');
				$mail = $this->request->getPost('email');
				$subject = $this->request->getPost('subject');
				$msg = $this->request->getPost('message');

				$message = "Name : ".$name."<br>";
				$message .= "Email-id : ".$mail."<br>";
				$message .= "Message Given : ".$msg."<br>";

				$email->setSubject($subject);
				$email->setMessage($message);
				$email->send();
				
				$session->setFlashdata('success', 'Message sent successfully. We will get back you soon...!');
				return redirect()->to('/UserController/contact');
			}
			else
			{
				//validation error
				$data['validation'] = $this->validator;
				return view('user/contactUs', $data);
			}
		}

		return view('user/contactUs');
	}


	
}
