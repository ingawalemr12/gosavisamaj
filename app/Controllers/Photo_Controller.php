<?php

namespace App\Controllers;
use App\Models\Image_Model;

class Photo_Controller extends BaseController
{
	public function index()
	{
		$pager = \Config\Services::pager();//pagination

		$model = new Image_Model();
		// $photos['photos'] = $model->findAll();

		$photos['photos'] = $model->paginate(6);
		$photos['pagination_link'] = $model->pager;
		//echo "<pre>";print_r($photos); echo "</pre>"; exit();
		return view('user/photos', $photos);
	}

	public function details($id)
	{
		

		$model = new Image_Model(); 
		$query= $model->where('id', $id)->findAll();
		// echo "<pre>";print_r($query); echo "</pre>"; exit();
		
		if (empty($query)) {
			return redirect()->to('/Photo_Controller/index');
		}
		
		$data['query'] =$query;
		return view('user/photos_in_detials', $data);

		// $model = new Image_Model(); 
		// $data['photos_details']= $model->where('id', $id)->findAll();
		// // echo "<pre>";print_r($data); echo "</pre>"; exit();
		// return view('user/photos_details', $data);

	}

}








