<?php

namespace App\Controllers;

class Nath_Controller extends BaseController
{
	public function index()
	{
		return view('user/nath_sampraday');
	}

	public function nath_machindra()
	{
		return view('user/nath_machindra');
	}

	public function nath_gorakh()
	{
		return view('user/nath_gorakh');
	}

	public function nath_gahini()
	{
		return view('user/nath_gahini');
	}

	public function nath_jalindar()
	{
		return view('user/nath_jalindar');
	}

	public function nath_kanif()
	{
		return view('user/nath_kanif');
	}

	public function nath_bharti()
	{
		return view('user/nath_bharti');
	}

	public function nath_reven()
	{
		return view('user/nath_reven');
	}

	public function nath_nag()
	{
		return view('user/nath_nag');
	}

	public function nath_charpati()
	{
		return view('user/nath_charpati');
	}

	
}

