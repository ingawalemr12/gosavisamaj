<?php

namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (file_exists(SYSTEMPATH . 'Config/Routes.php'))
{
	require SYSTEMPATH . 'Config/Routes.php';
}

/**
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('UserController');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
$routes->setAutoRoute(true);

/*
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.
$routes->get('/', 'UserController::index');
$routes->get('home', 'UserController::index');

// Nath-Sampraday details
$routes->get('Nath-Sampraday', 'Nath_Controller::index');
$routes->get('machindra-nath', 'Nath_Controller::nath_machindra');
$routes->get('gorakh-nath', 'Nath_Controller::nath_gorakh');
$routes->get('gahini-nath', 'Nath_Controller::nath_gahini');
$routes->get('jalindar-nath', 'Nath_Controller::nath_jalindar');
$routes->get('kanif-nath', 'Nath_Controller::nath_kanif');
$routes->get('bharti-nath', 'Nath_Controller::nath_bharti');
$routes->get('reven-nath', 'Nath_Controller::nath_reven');
$routes->get('nag-nath', 'Nath_Controller::nath_nag');
$routes->get('charpati-nath', 'Nath_Controller::nath_charpati');

$routes->get('nath-panthi-video', 'Video_FController::index');
// photos details- frontend
$routes->get('nath-panthi-photo', 'Photo_Controller::index');
$routes->get('photo-details', 'Photo_Controller::Photos_details');


// Nath-panthi samaj details 	// nati details
$routes->get('nath-panthi', 'NathPanthiSamaj_Controller::index');
$routes->get('nati', 'NathPanthiSamaj_Controller::nati');



// admin home controller files
$routes->get('admin', 'admin/Admin_Controller::index');
// admin image Upload controller files
$routes->get('admin/image-upload', 'admin/Image_Controller::index');
$routes->post('image-store', 'admin/Image_Controller::imageStore');


// admin image video controller files
$routes->get('admin/video-upload', 'admin/Video_Controller::index');
// admin blog video controller files
$routes->get('admin/blog', 'admin/Blog_Controller::index');


/*
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (file_exists(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php'))
{
	require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
