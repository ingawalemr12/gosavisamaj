<?php
namespace App\Models;

use CodeIgniter\Model;

class Image_Model extends Model{
    
    //protected $db;
    protected $table = 'photos_details';
    protected $primaryKey = 'id';
    protected $allowedFields = ['mobile', 'name', 'city', 'image', 'description'];

    //protected $createdField = 'created_at';
   // protected $updatedField = 'updated_at';
}
