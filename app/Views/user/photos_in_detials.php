<!DOCTYPE html>
<html>
	
<head>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<title> नाथपंथी ( डवरी ) गोसावी समाज </title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
		<!-- Favicone Icon -->
		<link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url() ?>/public/assets/favicon.jpg">
		<!-- CSS -->
		<link href="https://fonts.googleapis.com/css?family=Raleway:300,400,700,800%7CLato:300,400,700%7CRoboto:400,500,700" rel="stylesheet" type="text/css">
		<link href="<?php echo base_url() ?>/public/assets/css/bootstrap.css" rel="stylesheet" type="text/css">
		<link href="<?php echo base_url() ?>/public/assets/css/font-awesome.css" rel="stylesheet" type="text/css">
		<link href="<?php echo base_url() ?>/public/assets/css/ionicons.css" rel="stylesheet" type="text/css">
		<link href="<?php echo base_url() ?>/public/assets/css/flaticon.css" rel="stylesheet" type="text/css">
		<!-- carousel -->
		<link href="<?php echo base_url() ?>/public/assets/css/owl.carousel.css" rel="stylesheet" type="text/css">
		<!-- mediaelementplayer -->
		<link href="<?php echo base_url() ?>/public/assets/css/mediaelementplayer.css" rel="stylesheet" type="text/css">
		<!--Light box--> 
		<link href="<?php echo base_url() ?>/public/assets/css/jquery.fancybox.css" rel="stylesheet" type="text/css">
		<!-- Revolution Style-sheet -->
		<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>/public/assets/rs-slider/rs-plugin/css/settings.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>/public/assets/css/rev-style.css">
		<!--Main Style-->
		<link href="<?php echo base_url() ?>/public/assets/css/nav_corporate.css" rel="stylesheet" type="text/css">
		<link href="<?php echo base_url() ?>/public/assets/css/style.css" rel="stylesheet" type="text/css">
		<!--Theme Color-->
		<link href="<?php echo base_url() ?>/public/assets/css/theme-color/default.css" rel="stylesheet" id="theme-color" type="text/css">
	<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-119595512-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-119595512-1');
</script>

</head>

<body class="full-intro background--dark">
	<!--Start header area-->
	<header class="header-area">
				<div class="container">
					<div class="row">
						<div class="col-md-3 col-sm-12 col-xs-12">
							<div class="logo">
								<img src="<?php echo base_url() ?>/public/assets/images/logo-black.png" alt="Awesome Logo">
							</div>
						</div>
						<div class="col-md-9 col-sm-12 col-xs-12">
							<div class="header-contact-info">
								<ul>
									<li>
										<div class="iocn-holder">
											<span class="fa fa-home"></span>
										</div>
										<div class="text-holder">
											<h6>Shirwal, 412801</h6>
											<p>
												District-Satara ( MH )
											</p>
										</div>
									</li>
									<li>
										<div class="iocn-holder">
											<span class="fa fa-phone-square"></span>
										</div>
										<div class="text-holder">
											<h6>Call Us On</h6>
											<p>
												+91 970410333
											</p>
										</div>
									</li>
									<li>
										<div class="iocn-holder">
											<span class="fa fa-envelope"></span>
										</div>
										<div class="text-holder">
											<h6>Mail Us </h6>
											<a href="#">
											<p>
												ingawalemr12@gmail.com
											</p> </a>
										</div>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
	</header>
	<!--End header area-->
	<!--Start mainmenu area-->
			<section class="mainmenu-area">
				<div class="container">
					<div class="mainmenu-bg">
						<div class="row">
							<div class="col-md-9 col-sm-12 col-xs-12">
								<!--Start mainmenu-->
								<nav class="main-menu">
									<div class="navbar-header">
										<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
											<span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
										</button>
									</div>
									<div class="navbar-collapse collapse clearfix">
										<ul class="navigation clearfix">
											<li>
												<a href="<?php echo base_url('home') ?>">Home</a>
											</li>
											<li>
												<a href="<?php echo base_url('nati') ?>">
												नाती</a>
											</li>
											<li class="nav-has-sub">
												<a href="#!">जमाती</a>
											</li>
											<li>
												<a href="<?php echo base_url('Nath-Sampraday'); ?>">नाथ संप्रदाय</a>
											</li>
											<li>
												<a href="<?php echo base_url('nath-panthi-photo'); ?>">Photo
												</a>
											</li>
											<li>
												<a href="<?php echo base_url('nath-panthi-video'); ?>">Video
												</a>
											</li>

											<li>
												<a href="about.html">Blog</a>
											</li>
											
										</ul>

										<!-- ==============================
										=========Mobile Navigation==========
										==================================== -->
										<ul class="mobile-menu clearfix">
											<li>
												<a href="<?php echo base_url('home') ?>">Home</a>
											</li>
											
											<li>
												<a href="<?php echo base_url('nati') ?>">नाती </a>
											</li>

											<li>
												<a href="#!">जमाती</a>
											</li>
											<li>
												<a href="<?php echo base_url('Nath-Sampraday'); ?>">नाथ संप्रदाय</a>
											</li>
											<li class="nav-has-sub">
												<a href="<?php echo base_url('nath-panthi-photo'); ?>">Photo
												</a>
											</li>
											<li>
												<a href="<?php echo base_url('nath-panthi-video'); ?>">Video
												</a>
											</li>
											<li>
												<a href="about.html">Blog</a>
											</li>
											
										</ul>
									</div>
								</nav>
								<!--End mainmenu-->

							</div>
						</div>
						<div class="right-column">
							<div class="right-area">
								<div class="nav_side_content">
									<div class="search_option">
										<button class="search tran3s dropdown-toggle color1_bg" id="searchDropdown">
											<i class="fa fa-search" aria-hidden="true"></i>
										</button>
										<form action="#" class="dropdown-menu dropdown-input" >
											<input type="text" placeholder="Search...">
											<button class="close-input">
												<i class="fa fa-close" aria-hidden="true"></i>
											</button>
										</form>
									</div>
								</div>
								<div class="link_btn float_right">
								<a href="contact.html" class="thm-btn bg-clr1">Reach Us</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<!--End mainmenu area-->
<!-- 
<section class="inner-intro  padding ptb-xs-40 bg-img1 overlay-dark light-color">
	<div class="container">
		<div class="row title">
			<h1>नाथपंथी डवरी गोसावी छायाचित्र</h1>
			<div class="page-breadcrumb">
				<a>index</a>/<span>नाथपंथी डवरी गोसावी छायाचित्र</span>
			</div>
		</div>
	</div>
</section> -->
<!-- End Intro Section -->

<section class="course-section__block event-sid padding blog-post" style="padding-top: 10px;">
	<div class="container">
			<?php 
				if (!empty($query)) 
				{
					foreach ($query as $row) {	?>
		<div class="row">
			<div class="col-sm-12">
				<div class="plan-content-box__white__bg">
					<div class="row mt-10">
						<div class="col-sm-4 pr-0">
							<div class="table__block">
							<?php
								helper('text');
								$path = './public/assets/images/adminPhotos/'.$row['image'];
									
								if ($row['image'] !="" && file_exists($path)) 
									{  ?>
									<img src="<?php echo base_url().'/public/assets/images/adminPhotos/'.$row['image'] ?>" height="300px;" width="100%">
									<?php	} else {	?>
									<img src="<?php echo base_url().'/public/assets/images/adminPhotos/no_image.jpg' ?>" alt="">
								<?php	} ?>
							</div>
										
							<div class="book__now">
								<a href="" class="btn-text width-100per"><?php echo "Mobile Number : ".$row['mobile']; ?>  </a>
							</div>
						</div>

						<div class="col-sm-8 right_border ">
							<div class="single_plan__block after__none">
								<div class="plan-content-box plan-content-box__wight__bg width-100per">
									<h2><?php echo $row['name'] ?>, <?php echo $row['city']; ?></h2>
									<p><?php echo $row['description']; ?></p><br>
										 
									<blockquote class="dark-bg" style="background-color: lightslategray;">
										Posted by  :
									    	<strong> 
									    		<?php echo $row['name'] ?>, <?php echo $row['city']; ?> ,
								    		</strong> on 
								    		<strong> <?php echo date('d M Y', strtotime($row['created_at']));?>
								    		</strong>
									</blockquote>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="col-sm-12">
				<div class="plan-content-box__white__bg">
					<div class="row mt-20">
						<div class="col-sm-12 pr-0">
							<div class="table__block">
								<?php
								helper('text');
								$path = './public/assets/images/adminPhotos/'.$row['image'];
									
								if ($row['image'] !="" && file_exists($path)) 
									{  ?>
									<img src="<?php echo base_url().'/public/assets/images/adminPhotos/'.$row['image'] ?>" height="470px;" width="100%">
									<?php	} else {	?>
									<img src="<?php echo base_url().'/public/assets/images/adminPhotos/no_image.jpg' ?>" alt="">
								<?php	} ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php  } } ?>
	</div>
</section>


		<!-- FOOTER -->
		<footer class="footer pt-80 pt-xs-60">
				<div class="container">
					<!--Footer Info -->
					<div class="row footer-info mb-60">
						<div class="col-md-3 col-sm-4 col-xs-12 mb-sm-30">
							<h4 class="mb-30">CONTACT Us</h4>
							<address>
								<i class="ion-ios-location fa-icons"></i> 
								Flat Nos.8, ShivParvati Biilding, Ambedkar Chowk, Shirwal , 412801
							</address>
							<ul class="link-small">
								<li>
									<a href="mailto:ingawalemr12@gmail.com"><i class="ion-ios-email fa-icons"></i>ingawalemr12@gmail.com</a>
								</li>
								<li>
									<a><i class="ion-ios-telephone fa-icons"></i>+91 9970410333</a>
								</li>
							</ul>
							<div class="icons-hover-black">
								<a href="javascript:avoid(0);"> <i class="fa fa-facebook"></i> </a><a href="javascript:avoid(0);"> <i class="fa fa-twitter"></i> </a><a href="javascript:avoid(0);"> <i class="fa fa-youtube"></i> </a><a href="javascript:avoid(0);"> <i class="fa fa-dribbble"></i> </a><a href="javascript:avoid(0);"> <i class="fa fa-linkedin"></i> </a>
							</div>
						</div>
						<div class="col-md-2 col-sm-3 col-xs-12 mb-sm-30">
							<h4 class="mb-30">Links</h4>
							<ul class="link blog-link">
								<li>
									<a href="javascript:avoid(0);"><i class="fa fa-angle-double-right"></i> इतर देशातील जमाती</a>
								</li>
								<li>
									<a href="javascript:avoid(0);"><i class="fa fa-angle-double-right"></i> About Us</a>
								</li>
								<li>
									<a href="javascript:avoid(0);"><i class="fa fa-angle-double-right"></i> Event Photo</a>
								</li>
								<li>
									<a href="javascript:avoid(0);"><i class="fa fa-angle-double-right"></i> Privacy policy</a>
								</li> 
								<li>
									<a href="javascript:avoid(0);"><i class="fa fa-angle-double-right"></i> Terms &amp; condition</a>
								</li>
							</ul>
						</div>
						<div class="col-md-3 col-sm-5 col-xs-12 mb-sm-30">
							<h4 class="mb-30">Latest Blog</h4>
							<div class="widget-details link">
								<div class="post-type-post media">
									<div class="entry-thumbnail media-left">
										<img src="<?php echo base_url() ?>/public/assets/images/blog/small-img.jpg" alt="Image">
									</div>
									<!-- /.entry-thumbnail -->
									<div class="post-content media-body">
										<p class="entry-title">
											<a href="javascript:avoid(0);">Sed vulputate odio dui vsagittis lorem etiverra elementum exfaucibus.</a>
										</p>
										<div class="post-meta">
											On
											<time datetime="2017-02-10">
												10 Feb, 2017
											</time>
										</div>
										<!-- /.post-meta -->
									</div>
									<!-- /.post-content -->
								</div>
								<div class="post-type-post media">
									<div class="entry-thumbnail media-left">
										<img src="<?php echo base_url() ?>/public/assets/images/blog/small-img1.jpg" alt="Image">
									</div>
									<!-- /.entry-thumbnail -->
									<div class="post-content media-body">
										<p class="entry-title">
											<a href="javascript:avoid(0);">Sed vulputate odio dui vsagittis lorem etiverra elementum exfaucibus.</a>
										</p>
										<div class="post-meta">
											On
											<time datetime="2017-02-10">
												10 Feb, 2017
											</time>
										</div>
										<!-- /.post-meta -->
									</div>
									<!-- /.post-content -->
								</div>
							</div>
						</div>
						<div class="col-md-4 col-sm-12 col-xs-12 mt-sm-30 mt-xs-30">
							<div class="newsletter">
								<h4 class="mb-30">NEWSLETTER SIGNUP</h4>
								<p>
									Subscribe to Our Newsletter to get Important News, Amazing Offers & Inside Scoops:
								</p>
								<form>
									<input type="email" class="newsletter-input input-md newsletter-input mb-0" placeholder="Enter Your Email">
									<button class="newsletter-btn btn btn-xs btn-color" type="submit" value="">
										<i class="ion-ios-paperplane mr-0"></i>
									</button>
								</form>
							</div>
						</div>
					</div>
					<!-- End Footer Info -->
				</div>
				<!-- Copyright Bar -->
				<div class="copyright">
					<div class="container">
						<p>
							&copy <?php echo date('Y'); ?> <!-- <a>Study Points</a> -->
							All Rights Reserved.
						</p>
					</div>
				</div>
				<!-- End Copyright Bar -->
		</footer>
			<!-- END FOOTER -->
		</div>
		<!-- Site Wraper End -->

		<script src="<?php echo base_url() ?>/public/assets/js/jquery-1.12.4.min.js" type="text/javascript"></script>
		<!-- masonry,isotope Effect Js -->
		<script src="<?php echo base_url() ?>/public/assets/js/imagesloaded.pkgd.min.js" type="text/javascript"></script>
		<script src="<?php echo base_url() ?>/public/assets/js/isotope.pkgd.min.js" type="text/javascript"></script>
		<script src="<?php echo base_url() ?>/public/assets/js/masonry.pkgd.min.js" type="text/javascript"></script>
		<script src="<?php echo base_url() ?>/public/assets/js/jquery.appear.js" type="text/javascript"></script>
		<!-- bootstrap Js -->
		<script src="<?php echo base_url() ?>/public/assets/js/bootstrap.min.js" type="text/javascript"></script>
		<!-- carousel Js -->
		<script src="<?php echo base_url() ?>/public/assets/js/plugin/owl.carousel.js" type="text/javascript"></script>
		<!-- fancybox Js -->
		<script src="<?php echo base_url() ?>/public/assets/js/jquery.mousewheel-3.0.6.pack.js" type="text/javascript"></script>
		<script src="<?php echo base_url() ?>/public/assets/js/jquery.fancybox.pack.js" type="text/javascript"></script>
		<!-- carousel Js -->
		<script src="<?php echo base_url() ?>/public/assets/js/jquery.parallax-1.1.3.js" type="text/javascript"></script>
		<!-- carousel Js -->
		<script src="<?php echo base_url() ?>/public/assets/js/mediaelement-and-player.min.js" type="text/javascript"></script>

		<!-- Form Js -->
		<script src="<?php echo base_url() ?>/public/assets/js/mail.js" type="text/javascript"></script>
		<!-- revolution Js -->
		<script type="text/javascript" src="<?php echo base_url() ?>/public/assets/rs-slider/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url() ?>/public/assets/rs-slider/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url() ?>/public/assets/js/revolution-custom.js"></script>
		<!-- Height Js -->
		<script src="<?php echo base_url() ?>/public/assets/js/jquery.matchHeight-min.js" type="text/javascript"></script>

		<!-- custom Js -->
		<!-- <script src="<?php //echo base_url() ?>/public/assets/js/custom.js" type="text/javascript"></script> -->
		<script src="<?php echo base_url().'/public/assets/js/hots_popup.js' ?>"></script>
		</body>

</html>