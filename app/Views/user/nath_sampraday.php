
<?= $this->extend('user/index') ?>

<?= $this->Section('nath_sampraday') ?>

	<section class="inner-intro  padding ptb-xs-40 bg-img1 overlay-dark light-color">
		<div class="container">
			<div class="row title">
				<h1>नाथ संप्रदाय - नवनाथ</h1>
					<div class="page-breadcrumb">
						<a>index</a>/<span>नाथ संप्रदाय </span>
					</div>
				</div>
			</div>
	</section>
  <!-- Intro Section --> 
<section class="course-section__block padding ptb-xs-60">
	<div class="container">
 		<div class="row">
			<div class="col-md-9 col-lg-9 mb-30">
				<div class="course__details_block">
					<div class="course__figure_img">
						<img src="<?php echo base_url(); ?>/public/assets/images/bg_11.jpg" alt="" />
					</div>
					<div class="course__text_details mt-40">
						<h1 class="mb-20">नवनाथ : नयन नारायणांचे अवतार हे नऊ नाथ आहेत.</h1>
						<p align="justify">
							नवनाथ बहुत दिवसपर्यंत तीर्थयात्रा करित होते. शके सत्राशें दहापर्यंत ते प्रकटरूपानें फिरत होते. नंतर गुप्त झाले. एका मठीमध्यें कानिफा राहिला. त्याच्याजवळ पण वरच्या बाजुनें मच्छिंद्रनाथ ज्याच्या बायबा असें म्हणतात तो राहिला. जालंदनाथास जानपीर म्हणतात, तो गर्भगिरोवर राहिला आणि त्याच्या खालच्या बाजूस गहिनीनाथ त्यासच गैरीपीर म्हणतात. वडवाळेस नागनाथ व रेवणनाथ वीटगांवीं राहिला चरपटीनाथ, चौरंगीनाथ व अंडबंगी नाथ गुप्तरुपानें अद्याप तीर्थयात्रा करीत आहेत. भर्तरी ( भर्तृहरि ) पाताळीं राहिला. मीननाथानें स्वर्गास जाऊन वास केला. गिरिनारपर्वतीं श्रीदत्तात्रेयाच्या आश्रमांत गोरक्षनाथ राहिला. गोपीचंद्र व धर्मनाथ हे वैकुंठास गेले. मग विष्णुनें विमान पाठवुन मैनावतीस वैकुंठास नेलें. चौऱ्यांयशीं सिद्धांपासुन नाथपंथ भरभराटीस आला.
						</p>
						
						<p align="justify">
							महाभारतमध्ये नवनाथ किंवा नऊ संदेष्ट्यांचा येत आहे. कृष्णाने पृथ्वीवरील आपले कार्य संपल्यानंतर चांगल्याप्रकारे प्रसार करण्याच्या त्याच्या संदेशाचे निरंतर पालन कसे करावे याचे संदेश देण्यासाठी सर्व देवता, देवदूतांना आणि संतांची एक बैठक बोलावली. कृष्णा म्हणाले की तो नऊ संत किंवा संदेष्ट्यांच्या स्वरूपात स्वतःचा प्रकाश पाठवेल, जे पृथ्वीच्या वेगवेगळ्या भागात आणि संपूर्ण विश्वामध्ये जिथे जिथे अस्तित्व आहे तिथे जाईल. भगवान कृष्ण यांनी असेही नमूद केले आहे की महावीष्णु किंवा शिव किंवा शक्ती यांच्यात एकत्र येण्याकरिता अडथळे दूर करून या संदेष्ट्यांनी किंवा संत प्रेम संदेश पसरवतील. कृष्णा म्हणाले की ज्यांना चांगले आत्मा आणि श्रद्धावान लोक आहेत केवळ त्यांनाच मदत होईल
						</p>
					</div>
					<div class="course__content_block mt-30">
						<h2 class="mb-20">नवनाथ</h2>
						<p align="justify">
							 कलियुगामध्ये भगवान नारायण यांचे पुनर्जन्म मानले जाते. भगवान कृष्ण यांनी त्यांना कलियुगामध्ये पुनर्जन्म करण्यास सांगितले. "कवी नारायण", "कवी नारायण", "हरि नारायण", गोरक्षनथ, "अंतिक्षा नारायण", जलदर्शनथ, "प्रबुद्ध नारायण", कानिफनाथ, "पिप्पलायन नारायण", चरपनाथ, "अविरोत्र नारायण", नागेशनाथ मध्ये पुनर्जन्म झाला. नारायण ", भारतीनथ," चामस नारायण ", रेवन्नथ, आणि" करभन नारायण ", गहिनीनाथमध्ये. नवनाथच्या जीवनाचे मुख्य ध्येय म्हणजे जनतेच्या कल्याणासाठी शबर मंत्र तयार करणे. हे नऊ संत अमर असल्याचे मानले जातात. निष्क्रिय झाल्यानंतर, त्यांच्या 84 शिष्यांनी नाथ संप्रदायाचे प्रचार केले आणि आजपर्यंत ते जगभरात एक अतिशय शक्तिशाली आणि लोकप्रिय संप्रदाय मानले जाते. महाराष्ट्र राज्यामध्ये नवनाथ सर्वात लोकप्रिय आणि पूजेचे आहेत.
						</p>
							<h2 class="mt-20">नवनाथच्या काही पवित्र स्थळे</h2>
							<ul class="course_features_point" style="width: 99%">
								<li><i class="fa fa-hand-o-right"></i> मच्छिंद्रनाथ किंवा मखेचेद्रनाथ: सावरगांव, तालुका: आष्टी, जिल्हा: बीड (महाराष्ट्र) </li>
								<li><i class="fa fa-hand-o-right"></i> गोरखनाथ किंवा गोरक्षनाथ: गिरनार, जुनागड (गुजरात)</li>
								<li><i class="fa fa-hand-o-right"></i> गहिनीनाथ: चिंचोर, तालुका: नेवासा, जिल्हा: अहमदनगर (महाराष्ट्र)</li>
								<li><i class="fa fa-hand-o-right"></i> जालिंदरनाथ / जालंधरथ किंवा जन पीअर: येओळवाडी, तालुका: पटोडा, जिल्हा: बीड (महाराष्ट्र)</li>
								<li><i class="fa fa-hand-o-right"></i>  कानिफनाथ: माही, तालुका: पाथर्डी, जिल्हा: अहमदनगर (महाराष्ट्र) </li>
								<li><i class="fa fa-hand-o-right"></i> भारतीनाथ. हरंगुल, तालुका: गंगाखेड, जिल्हा: परभणी (महाराष्ट्र)</li>
								<li><i class="fa fa-hand-o-right"></i> रेवननाथ: रेणावी, तालुका : खानापूर , जिल्हा: सांगली (महाराष्ट्र)</li>
								<li><i class="fa fa-hand-o-right"></i> नागनाथ / नागेश नाथ: वाडवाल, तालुका: मोहोळ, जिल्हा: सोलापूर (महाराष्ट्र)</li>
								<li><i class="fa fa-hand-o-right"></i> चरपनाथ: पवित्र स्थळ</li>
							</ul>
					</div>
				</div>
			</div>
			<!--Sidebar-->
			<div class="col-md-3 col-lg-3 mt-sm-60">
		        <div class="sidebar-widget">
		            <h4>Search</h4>
		            <div class="widget-search pt-15">
		              <input class="form-full input-lg" type="text" value="" placeholder="Search Here" name="search" id="wid-search">
		              <input type="submit" value="" name="email" id="wid-s-sub">
		            </div>
		        </div>
	            <div class="sidebar-widget">
	            	<h4>नाथ संप्रदाय :  नवनाथ </h4>
	            
		            <ul class="categories">
		              	<li>
		              		<a target="_blank" href="<?php echo base_url('machindra-nath'); ?>"><i class="fa fa-chevron-right"></i> मच्छिंद्रनाथ किंवा मत्स्येंद्रनाथ</a>
		              	</li>
						<li>
							<a target="_blank" href="<?php echo base_url('gorakh-nath'); ?>"><i class="fa fa-chevron-right"></i> गोरक्षनाथ किंवा गोरखनाथ</a>
						</li>
						<li>
							<a target="_blank" href="<?php echo base_url('gahini-nath'); ?>"><i class="fa fa-chevron-right"></i> गहिनीनाथ  किंवा  गाईबी पीअर</a></li>
						<li>
							<a target="_blank" href="<?php echo base_url('jalindar-nath'); ?>"><i class="fa fa-chevron-right"></i> जालिंदरनाथ किंवा जालदारनाथ</a>
						</li>
						<li>
							<a target="_blank" href="<?php echo base_url('kanif-nath'); ?>"><i class="fa fa-chevron-right"></i>कानिफनाथ किंवा कान्होबा</a>
						</li>
						<li>
							<a target="_blank" href="<?php echo base_url('bharti-nath'); ?>"><i class="fa fa-chevron-right"></i> भारतीनाथ किंवा भर्तरीनाथ</a>
						</li>
						<li>
							<a target="_blank" href="<?php echo base_url('reven-nath'); ?>"><i class="fa fa-chevron-right"></i> रेवणनाथ किंवा रीवन सिद्ध किंवा काडा सिद्ध किंवा रावलनाथ</a>
						</li>
						<li>
							<a target="_blank" href="<?php echo base_url('nag-nath'); ?>"><i class="fa fa-chevron-right"></i> नागनाथ किंवा नागेशनाथ</a>
						</li>
						<li>
							<a target="_blank" href="<?php echo base_url('charpati-nath'); ?>"><i class="fa fa-chevron-right"></i> चरपटीनाथ</a>
						</li>
		            </ul>
	            </div>
			    <div class="sidebar-widget">
	            	<h4>Course Type</h4>
	            
	            	<ul class="categories">
	        			<li><a href="#"><i class="fa fa-chevron-right"></i> all</a></li>
						<li><a href="#"><i class="fa fa-chevron-right"></i> paid</a></li>
						<li><a href="#"><i class="fa fa-chevron-right"></i> free</a></li>
	            	</ul>
	          	</div>
			  	<div class="sidebar-widget">
	            	<h4>Course Certification</h4>
	            
	            	<ul class="categories">
						<li><a href="#"><i class="fa fa-chevron-right"></i> diploma</a></li>
						<li><a href="#"><i class="fa fa-chevron-right"></i> graduation</a></li>
						<li><a href="#"><i class="fa fa-chevron-right"></i> post graduation</a></li>
	            	</ul>
	          	</div>
			   
	        </div>
			<!--Sidebar-->
		</div>
	</div>
	</section>		
<?= $this->endSection() ?>
