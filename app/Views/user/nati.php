
<?= $this->extend('user/index') ?>

<?= $this->Section('nath_sampraday') ?>

	<section class="inner-intro  padding ptb-xs-40 bg-img1 overlay-dark light-color">
		<div class="container">
			<div class="row title">
				<h1>नाथपंथी डवरी गोसावी - नाती</h1>
					<div class="page-breadcrumb">
						<a>index</a>/<span>नाती </span>
					</div>
				</div>
			</div>
	</section>
  <!-- Intro Section --> 
<section class="course-section__block padding ptb-xs-60">
	<div class="container">
 		<div class="row">
			<div class="col-md-9 col-lg-9 mb-30">
				<div class="course__details_block">
					
					<div class="course__text_details mt-40">
						<h2 class="mb-20">
							नाती ( 9 ) , भावंडे( 6 ) , बहिणी( 16 ) ,  कायदा( 32 ) , पाखंडे( 36 )
						</h2>
						<p align="justify">
							भटक्या जमातीतील नाथपंथी डवरी गोसावी ही एक जमात.  यांच्यापकी सुमारे ६० टक्के आजही विकासप्रक्रियेपासून अलिप्त असे भटके जीवन जगत आहेत. डवरी गोसावी, नाथ जोगी, जोगी, गारपगारी, बालसंतोषी, किंगरीवाले, भराडी वगैरे नावाने ओळखले जाणारे वेगवेगळे गट पडले म्हणजेच त्या नावांच्या छोटय़ा छोटय़ा जमाती निर्माण झाल्या.  </p>
							<p align="justify">उत्तर प्रदेशमधील गोरखपूर किंवा महाराष्ट्रातील मढी येथील गोरक्षनाथाला मानणारे 'नाथजोगी', तर उस्मानाबाद जिल्ह्य़ातील सोनारी येथील भरवनाथाची गादी (गुरूला गादी म्हटले जाते.) व नाथांचा आखाडा मानणारे 'नाथपंथी डवरी गोसावी' असे महाराष्ट्रात प्रामुख्याने दोन गट म्हणजेच जमाती आढळतात.</p>
							<p align="justify">
							"भटक्या" हा शब्द प्राचीन ग्रीक या शास्त्रीय ग्रीक शब्दापासून (नामांक, "फिरणे, भटकणे, विशेषत: कुरण शोधण्यासाठी") आला आहे. भटक्या विमुक्त लोक पारंपारिकपणे प्राणी किंवा डोंगर किंवा पायी प्रवास करतात. आज काही भटक्या मोटर वाहनातून प्रवास करतात. 
						</p>
					</div>
					<div class="course__content_block mt-30">
						<h2 class="mb-20">नाती ( 9 )  - खालीलप्रमाने</h2>
						<p align="justify">
							<ol>
								<li> ईन बायको </li>
								<li> सासू बायको </li>
								<li> मावशी  बायको </li>
								<li> मामी  बायको </li>
								<li> भावजय  बायको </li>
								<li> मेव्हणी  बायको </li>
								<li> हटक नाते बायको </li>
								<li> लेक/मुलगी बायको </li>
								<li> मावळण बायको </li>
							</ol>
							<strong><mark>7 नाती बंद   ( 7 Relationship's are closed )</mark></strong>
						</p>

						<h2 class="mb-20 pt-20">भावंडे( 6 )  - खालीलप्रमाने </h2>
						<p align="justify">
							<ol>
								<li> सखा भाऊ</li>
								<li> चुलत भाऊ  </li>
								<li> दूध भाऊ</li>
								<li> मावस भाऊ  </li>
								<li> राय भाऊ</li>
								<li> साडू भाऊ  </li>
							</ol>
						</p>

						<h2 class="mt-20">बहिणी ( 16 ) - खालीलप्रमाने</h2>
						<ul class="course_features_point" style="width: 99%">
								<li><i class="fa fa-hand-o-right"></i>ईन बहीण  </li>
								<li><i class="fa fa-hand-o-right"></i>सासू बहीण </li>
								<li><i class="fa fa-hand-o-right"></i>मावशी   बहीण </li>
								<li><i class="fa fa-hand-o-right"></i> लाड बहीण  </li>
								<li><i class="fa fa-hand-o-right"></i> चुलत बहीण   </li>
								<li><i class="fa fa-hand-o-right"></i> मामी बहीण </li>
								<li><i class="fa fa-hand-o-right"></i>मावस बहीण   </li>
								<li><i class="fa fa-hand-o-right"></i>राय  बहीण  </li>
								<li><i class="fa fa-hand-o-right"></i>मेव्हणी  बहीण </li>
								<li><i class="fa fa-hand-o-right"></i>दूध    बहीण </li>
								<li><i class="fa fa-hand-o-right"></i> मावळण  बहीण  </li>
								<li><i class="fa fa-hand-o-right"></i> सखी  बहीण   </li>
								<li><i class="fa fa-hand-o-right"></i> भावजय  बहीण </li>
								<li><i class="fa fa-hand-o-right"></i>लेक/मुलगी बहीण   </li>
								<li><i class="fa fa-hand-o-right"></i>हटक नाते बहीण  </li>
								<li><i class="fa fa-hand-o-right"></i>भाची बहीण </li>
						</ul>
						<strong><mark>6  नाती बंद   (6 Relationship's are closed )</mark></strong>
						<h2 class="mt-20">कायदा ( 32 )</h2>
						<h2 class="mt-20">पाखंडे ( 36 )</h2>
					</div>
					<!-- <div class="course__figure_img" style="margin-top: 20px;">
						<img src="<?php echo base_url(); ?>/public/assets/images/bg_11.jpg" alt="" />
					</div> -->
				</div>
			</div>
			<!--Sidebar-->
			<div class="col-md-3 col-lg-3 mt-sm-60">
		        <div class="sidebar-widget">
		            <h4>Search</h4>
		            <div class="widget-search pt-15">
		              <input class="form-full input-lg" type="text" value="" placeholder="Search Here" name="search" id="wid-search">
		              <input type="submit" value="" name="email" id="wid-s-sub">
		            </div>
		        </div>
	            <div class="sidebar-widget">
	            	<h2>जमाती :  कूळ </h2>
	            
		            <ul class="categories">
		              	<li>
		              		<a href="#"><i class="fa fa-chevron-right"></i> इंगवले / इंगोले - सांगोला   </a>
		              	</li>
						<li>
							<a  href="#"><i class="fa fa-chevron-right"></i> भोसले / खरदारी ( शिंगणापूर )  </a>
						</li>
						<li>
							<a href="#"><i class="fa fa-chevron-right"></i> बाबर / साळुंखे ( व्रणे-आबापुरी )  </a>
						</li>
						<li>
							<a  href="#"><i class="fa fa-chevron-right"></i> तांबवा ( बावधन )</a>
						</li>
						<li>
							<a href="#"><i class="fa fa-chevron-right"></i> बोडका ( पाटण ) </a>
						</li>
						<li>
							<a href="#"><i class="fa fa-chevron-right"></i> रोतीचा शितोला ( भूड )   </a>
						</li>
						<li>
							<a  href="#"><i class="fa fa-chevron-right"></i> आहेर ( जलुबावी )</a>
						</li>
						<li>
							<a href="#"><i class="fa fa-chevron-right"></i>  मालवे / बुरुंगले ( वीर )  </a>
						</li>
						<li>
							<a  href="#"><i class="fa fa-chevron-right"></i> शेगर ( मेडद ) </a>
						</li>
						<li>
							<a href="#"><i class="fa fa-chevron-right"></i> चव्हाण ( नाझगिरी )</a>
						</li>
						<li>
							<a href="#"><i class="fa fa-chevron-right"></i> चौगुले / कोष्टा ( बलवडी ) </a>
						</li>
						<li>
							<a href="#"><i class="fa fa-chevron-right"></i> लगस ( जांब ) </a>
						</li>
						<li>
							<a href="#"><i class="fa fa-chevron-right"></i>जगताप </a>
						</li>
						<li>
							<a href="#"><i class="fa fa-chevron-right"></i>कदम </a>
						</li>
						<li>
							<a href="#"><i class="fa fa-chevron-right"></i> गुंजाळ </a>
						</li>
						<li>
							<a href="#"><i class="fa fa-chevron-right"></i> निकम </a>
						</li>
		            </ul>
	            </div>
			    <div class="sidebar-widget">
	            	<h2> स्वतंत्र कूळ : शिंदे(14)</h2>
	            
	            	<ul class="categories">
	        			<li><a href="#"><i class="fa fa-chevron-right"></i> सूर्यनाथ / सुरनाथ शिंदे </a></li>
						<li><a href="#"><i class="fa fa-chevron-right"></i> झरेकर शिंदे</a></li>
						<li><a href="#"><i class="fa fa-chevron-right"></i> मार्ग| / मारगा शिंदे</a></li>
						<li><a href="#"><i class="fa fa-chevron-right"></i> अरपता शिंदे </a></li>
						<li><a href="#"><i class="fa fa-chevron-right"></i> परपता शिंदे</a></li>
						<li><a href="#"><i class="fa fa-chevron-right"></i> स्वरपता शिंदे</a></li>
						<li><a href="#"><i class="fa fa-chevron-right"></i> रोटा शिंदे </a></li>
						<li><a href="#"><i class="fa fa-chevron-right"></i> नागनाथ शिंदे</a></li>
						<li><a href="#"><i class="fa fa-chevron-right"></i> मांडवकर शिंदे</a></li>
						<li><a href="#"><i class="fa fa-chevron-right"></i> खिलार शिंदे </a></li>
						<li><a href="#"><i class="fa fa-chevron-right"></i> जावचा शिंदे</a></li>
						<li><a href="#"><i class="fa fa-chevron-right"></i> Raksa शिंदे</a></li>
	            	</ul>
	          	</div>
			  	
	        </div>
			<!--Sidebar-->
		</div>
	</div>
	</section>		
<?= $this->endSection() ?>
