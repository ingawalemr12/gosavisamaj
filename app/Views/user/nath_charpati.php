
<?= $this->extend('user/index') ?>

<?= $this->Section('nath_sampraday') ?>

<!-- Intro Section -->
			<section class="inner-intro  padding ptb-xs-40 bg-img1 overlay-dark light-color">
				<div class="container">
					<div class="row title">
						<h1>चरपटीनाथ</h1>
						<div class="page-breadcrumb">
							<a>index</a>/ <span>चरपटीनाथ</span>
						</div>
					</div>
				</div>
			</section>
			<!-- End Intro Section -->
			<!-- End Intro Section -->
			<div class="padding pt-xs-40">
				<div class="container">
					<div class="row">
						<div class="col-sm-12 ">
							<div class="heading-box pb-10">
								<h2> चरपटीनाथ</h2>
							</div>
							<div class="text-content">
								<p align="justify">									
 								श्रीमद भागवतात उल्लेख केल्या प्रमाणे श्री वृषभ देवांच्या शंभर पैकी "नऊ नारायण " त्यातील कवी नारायणाचे प्रथम अवतार असलेले श्री मत्स्येंद्रनाथ जी होय . 
								 श्री नवनाथ कथासार या मालू कवी विरचित दृष्टांत स्वरूप ग्रंथात उल्लेखित केल्याप्रमाणे कवी नारायणांनी मत्स्याच्या पोटी अवतार धारण केला आणि " श्री मत्स्येंद्र " हे नाम धारण केले . श्री मत्स्येंद्रनाथ जी हे नाथ पंथाचे आद्य नाथाचार्य होत . 
								 कौल मताचे व हठयोगाचे विवरण करणाऱ्या प्राचीनतम ग्रंथांपैकी एक असणाऱ्या कौलज्ञाननिर्णय नावाच्या संस्कृत ग्रंथाचे जनकत्व विद्वानांच्या मतांनुसार त्यांच्याकडे जाते.
								 मध्ययुगातील भक्तिचळवळींमध्ये प्रमुख भूमिका बजावणाऱ्या नाथ संप्रदायाचे ते संस्थापक मानले जातात.
								</p>
								<p align="justify"> 
									<strong>चरपटीनाथ महाराज हे नवनाथ महाराजां पैकी एक आहेत</strong>
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>

			<section class="course-section__block event-sid padding blog-post" style="padding-top: 0px;">
				<div class="container">
					<div class="row">
						<div class="col-sm-12">
							<div class="plan-content-box__white__bg">
								<div class="row mt-10">
									<div class="col-sm-4 pr-0">
										<div class="table__block">
											<img src="<?php echo base_url() ?>/public/assets/images/project1.jpg" alt="Image">
										</div>
										
										<div class="book__now padding-20 ">
											<a href="#" class="btn-text width-100per">चरपटीनाथ  </a>
										</div>
									</div>

									<div class="col-sm-8 right_border ">
										<div class="single_plan__block after__none">
											<div class="plan-content-box plan-content-box__wight__bg width-100per">
												<h2>नयन नारायणांचे अवतार हे नऊ नाथ आहेत.</h2>
												<p>
													कलियुगामध्ये भगवान नारायण यांचे पुनर्जन्म मानले जाते. भगवान कृष्ण यांनी त्यांना कलियुगामध्ये पुनर्जन्म करण्यास सांगितले. "कवी नारायण", "कवी नारायण", "हरि नारायण", गोरक्षनथ, "अंतिक्षा नारायण", जलदर्शनथ, "प्रबुद्ध नारायण", कानिफनाथ, "पिप्पलायन नारायण", चरपनाथ, "अविरोत्र नारायण", नागेशनाथ मध्ये पुनर्जन्म झाला. नारायण ", भारतीनथ," चामस नारायण ", रेवन्नथ, आणि" करभन नारायण ", गहिनीनाथमध्ये. नवनाथच्या जीवनाचे मुख्य ध्येय म्हणजे जनतेच्या कल्याणासाठी शबर मंत्र तयार करणे. हे नऊ संत अमर असल्याचे मानले जातात. निष्क्रिय झाल्यानंतर, त्यांच्या 84 शिष्यांनी नाथ संप्रदायाचे प्रचार केले आणि आजपर्यंत ते जगभरात एक अतिशय शक्तिशाली आणि लोकप्रिय संप्रदाय मानले जाते. महाराष्ट्र राज्यामध्ये नवनाथ सर्वात लोकप्रिय आणि पूजेचे आहेत.
												</P>

												<blockquote class="dark-bg">
													चरपटीनाथ
												</blockquote>
											</div>
										</div>
									</div>
								</div>
							</div>

						</div>

					</div>

				</div>
			</section>
			<!--End Contact-->
<?= $this->endSection() ?>
