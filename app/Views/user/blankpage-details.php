
<?= $this->extend('user/index') ?>

<?= $this->Section('nath_sampraday') ?>

	<section class="inner-intro  padding ptb-xs-40 bg-img1 overlay-dark light-color">
		<div class="container">
			<div class="row title">
				<h1>नाथ संप्रदाय - नवनाथ</h1>
					<div class="page-breadcrumb">
						<a>index</a>/<span>नाथ संप्रदाय </span>
					</div>
				</div>
			</div>
	</section>
  <!-- Intro Section --> 
<section class="course-section__block padding ptb-xs-60">
	<div class="container">
 		<div class="row">
			<div class="col-md-12 ">
				<div class="course__details_block">
					<div class="course__figure_img">
						<img src="<?php echo base_url(); ?>/public/assets/images/bg_11.jpg" alt="" />
					</div>
					<div class="course__text_details mt-40">
						<h2 class="mb-20">नवनाथ : नयन नारायणांचे अवतार हे नऊ नाथ आहेत.</h1>
						<p align="justify">
							नवनाथ बहुत दिवसपर्यंत तीर्थयात्रा करित होते. शके सत्राशें दहापर्यंत ते प्रकटरूपानें फिरत होते. नंतर गुप्त झाले. एका मठीमध्यें कानिफा राहिला. त्याच्याजवळ पण वरच्या बाजुनें मच्छिंद्रनाथ ज्याच्या बायबा असें म्हणतात तो राहिला. जालंदनाथास जानपीर म्हणतात, तो गर्भगिरोवर राहिला आणि त्याच्या खालच्या बाजूस गहिनीनाथ त्यासच गैरीपीर म्हणतात. 
						</p>
					
					</div>
					<div class="course__content_block mt-30">
						<h2 class="mb-20">नवनाथ</h2>
						<p align="justify">
							 कलियुगामध्ये भगवान नारायण यांचे पुनर्जन्म मानले जाते. भगवान कृष्ण यांनी त्यांना कलियुगामध्ये पुनर्जन्म करण्यास सांगितले. 
						</p>
							<h2 class="mt-20">नवनाथच्या काही पवित्र स्थळे</h2>
							<ul class="course_features_point" style="width: 99%">
								<li><i class="fa fa-hand-o-right"></i>  कानिफनाथ: माही, तालुका: पाथर्डी, जिल्हा: अहमदनगर (महाराष्ट्र) </li>
							</ul>
					</div>
				</div>
			</div>
			
		</div>
	</div>
	</section>		
<?= $this->endSection() ?>
