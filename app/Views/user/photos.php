<?= $this->extend('user/index') ?>

<?= $this->Section('nathPanthi_samaj') ?>

<section class="inner-intro  padding ptb-xs-40 bg-img1 overlay-dark light-color">
	<div class="container">
		<div class="row title">
			<h1>नाथपंथी डवरी गोसावी छायाचित्र</h1>
			<div class="page-breadcrumb">
				<a>index</a>/<span>नाथपंथी डवरी गोसावी छायाचित्र</span>
			</div>
		</div>
	</div>
</section>
<!-- End Intro Section -->

<div class="padding pt-xs-40" >
	<div class="container">
		<div class="row">
			<div class="col-sm-12 ">
				<div class="heading-box pb-10">
					<h2> छायाचित्र</h2>
				</div>
				<div class="text-content">
					<p align="justify">									
	 				<strong>महाराष्ट्रातील भटक्या जमातीतील नाथपंथी डवरी गोसावी ही एक जमात आहे.</strong> भिक्षा मागताना ते डमरू (डौर) वाजवीत असल्यामुळे डवरी गोसावी असेही म्हणतात. प्राचीनकाळी एकसंध असलेल्या नाथ संप्रदायाचे गुरुनिहाय, प्रांतनिहाय गट पडले आणि कालांतराने त्या गटांच्या जाती-जमाती बनल्या. उत्तर प्रदेशमधील गोरखपूर किंवा महाराष्ट्रातील मढी येथील गोरक्षनाथाला मानणारे 'नाथजोगी', तर उस्मानाबाद जिल्ह्य़ातील सोनारी येथील भरवनाथाची गादी (गुरूला गादी म्हटले जाते.) व नाथांचा आखाडा मानणारे 'नाथपंथी डवरी गोसावी' असे महाराष्ट्रात प्रामुख्याने दोन गट म्हणजेच जमाती आढळतात.
					</p>
					
				</div>
			</div>
		</div>
	</div>
</div>

<!-- Service Section -->
<div id="services-section" >
	<div class="container">
		<div class="row">
			<?php
			if (!empty($photos)) {
				foreach ($photos as $photo) { ?>
			<div class="col-md-4 mb-30">
				<div class="item">
					<div class="clearfix bg-area">
						<div class="image-box">
							<?php
								helper('text');
								 $path = './public/assets/images/adminPhotos/'.$photo['image'];
								if ($photo['image'] !="" && file_exists($path)) {  ?>
									<img height="250" width="100" src="<?php echo base_url().'/public/assets/images/adminPhotos/'.$photo['image'] ?>" alt="">
								<?php	} else {	?>
									<img src="<?php echo base_url().'/public/assets/images/adminPhotos/no_image.jpg' ?>" alt="">
								<?php	} ?>
						</div>
						<div class="content-box">
							<div class="donate-price">
								Mobile Numbe : <?php echo $photo['mobile'] ; ?>
							</div>
							<h4><a href="#"><?php echo $photo['name'] ; ?>, <?php echo $photo['city'] ; ?></h4>

							<p><?php echo word_limiter(strip_tags($photo['description']), 5)   ; ?> </p>
							<div class="link">
								<a href="<?php echo base_url().'/Photo_Controller/details/'.$photo['id'] ?>" target="_blank" class="btn-text">Read More</a>
							</div>
						</div>
					</div>
				</div>
			</div>	
			<?php } } ?>
		</div>
	</div>
</div>
<!-- Service Section end -->
 
 <div class="pl-5">
 	<?php
    	if($pagination_link)
        {
            $pagination_link->setPath('Photo_Controller');// setPath is optional here
            echo $pagination_link->links();
        }
    ?>
<?= $this->endSection() ?>