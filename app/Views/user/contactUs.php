<?= $this->extend('user/index') ?>

<?= $this->section('contact') ?>

      <!-- Intro Section -->
      <section class="inner-intro  padding ptb-xs-40 bg-img1 overlay-dark light-color">
        <div class="container">
          <div class="row title">
            <h1>Contact</h1>
            <div class="page-breadcrumb">
              <a>index</a>/<span>Contact</span>
            </div>
          </div>
        </div>
      </section>
      <!-- End Intro Section -->
      <!-- Contact Section -->
      <section class="padding ptb-xs-40">
        <div class="container">

          <div class="row">
              <div class="col-sm-6 mt-10">
                    <?php 
                    $session = \Config\Services::session();
                    if (!empty($session->getFlashdata('success'))) { ?>
                      <div class="alert alert-danger">
                        <?php echo $session->getFlashdata('success'); ?>
                      </div>
                   <?php  }
                    ?>
              </div>
            <div class="col-sm-8">

              <div class="headeing pb-30">
                <h2>Get in Touch</h2>
              </div>
              
              <!-- Contact FORM -->
              <form action="<?php echo base_url('UserController/contact')?>"  class="contact-form"  method="post" >
                <?= csrf_field(); ?>
                <div class="row">
                  <div class="col-sm-6">
                    <div class="form-field">
                      <input type="text" name="name" placeholder="Your Name" class="input-sm form-full<?php echo (isset($validation) && $validation->hasError('name')) ? 'is-invalid' : '';?>" value="<?php echo set_value('name') ?>">
                      <?php
                      if (isset($validation) && $validation->hasError('name')) {
                        echo "<p class='invalid-feedback'>".$validation->getError('name')."</p>";
                      }
                       ?>

                    </div>
                    <div class="form-field">
                      <input class="input-sm form-full" type="text" name="email" placeholder="Email" value="<?php echo set_value('email') ?>" >
                      <?php 
                      if (isset($validation) && $validation->hasError('email')) {
                        echo $validation->getError('email');
                      }
                      ?>
                    </div>
                    <div class="form-field">
                      <input class="input-sm form-full" type="text" name="subject" id="subject" placeholder="Subject" value="<?php echo set_value('subject') ?>" >
                      <?php 
                      if (isset($validation) && $validation->hasError('subject')) {
                        echo $validation->getError('subject');
                      }
                      ?>
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-field">
                      <textarea class="form-full" rows="7" name="message" placeholder="Your Message" ></textarea>
                    </div>
                  </div>
                  <div class="col-sm-12 mt-30">
                  <input type="submit" name="submit" class="btn-text" value="Send Message">
                  </div>
                  
                </div>
              </form>
              <!-- END Contact FORM -->
            </div>

            <div class="col-sm-4 contact">
              <div class="headeing pb-20">
                <h2>Contact Info</h2>

              </div>
              <div class="contact-info">

                <ul class="info">
                  <li>
                    <div class="icon ion-ios-location"></div>
                    <div class="content">
                      <p>
                        Flat Nos.8, Shiv-Parvati Building,
                      </p>
                      <p>
                        Ambedkar Chowk, Shirwal , 412801.
                      </p>
                    </div>
                  </li>

                  <li>
                    <div class="icon ion-android-call"></div>
                    <div class="content">
                      <p>
                       +91 9970410333
                      </p>
                      <p>
                        +91 7775915269
                      </p>
                    </div>
                  </li>
                  <li>
                    <div class="icon ion-ios-email"></div>
                    <div class="content">
                      <p>
                        ingawalemr12@gmail.com
                      </p>
                      <p>
                        sidstechdigital@gmail.com
                      </p>
                    </div>
                  </li>
                  <li>
                    <div class="icon ion-clock"></div>
                    <div class="content">
                      <p>
                        Monday - Saturday
                      </p>
                      <p>
                        8:00 AM to 7:00 PM
                      </p>
                    </div>
                  </li>
                </ul>
                <ul class="event-social">
                  <li>
                    <a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                  </li>
                  <li>
                    <a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                  </li>
                  <li>
                    <a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                  </li>
                  <li>
                    <a href="#"><i class="fa fa-dribbble" aria-hidden="true"></i></a>
                  </li>
                  <li>
                    <a href="#"><i class="fa fa-pinterest-p" aria-hidden="true"></i></a>
                  </li>
                  <li>
                    <a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a>
                  </li>
                </ul>
              </div>
            </div>

          </div>
        </div>
        <!-- Map Section -->

      </section>
      <!-- Map -->
      <div class="container">
        <section class="map-box">
          <div class="map">
            <div id="map">
              <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d30331.634517651597!2d73.95717507575164!3d18.142973758305956!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bc2f64a1297f7f3%3A0xaac43a1e1605c70e!2sShirwal%2C%20Maharashtra!5e0!3m2!1sen!2sin!4v1624448022356!5m2!1sen!2sin" width="100%" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
            </div>
          </div>
        </section>
      </div><br>
      <!-- Contact Section -->

<?= $this->endSection() ?>