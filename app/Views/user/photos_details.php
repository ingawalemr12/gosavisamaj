
<?= $this->extend('user/index') ?>

<?= $this->Section('nath_sampraday') ?>

	<section class="inner-intro  padding ptb-xs-40 bg-img1 overlay-dark light-color">
		<div class="container">
			<div class="row title">
				<h1>नाथ संप्रदाय - नवनाथ</h1>
					<div class="page-breadcrumb">
						<a>index</a>/<span>नाथ संप्रदाय </span>
					</div>
				</div>
			</div>
	</section>
  <!-- Intro Section --> 
<section class="course-section__block padding ptb-xs-60">
	<div class="container">
	<?php 
		if (!empty($photos_details)) {
			foreach ($photos_details as $row) {
				 ?>
		<div class="row">
			<div class="col-md-12 ">
				<div class="course__details_block">
					<div class="course__figure_img">
						<?php
						helper('text');
						$path = './public/assets/images/adminPhotos/'.$row['image'];
						
						if ($row['image'] !="" && file_exists($path)) 
						{  ?>
							<img height="250" width="100" src="<?php echo base_url().'/public/assets/images/adminPhotos/'.$row['image'] ?>" alt="">
				<?php	}
						else {	?>
							<img src="<?php echo base_url().'/public/assets/images/adminPhotos/no_image.jpg' ?>" alt="">
						<?php	} ?>
					</div>
					<div class="course__text_details mt-40">
						<h2 class="mb-20"><?php echo $row['name'] ?></h2>
							<?php echo $row['mobile']; ?><br>
							<?php echo $row['city']; ?>
					</div>
					<div class="course__content_block mt-30">
						<h2 class="mb-20">नवनाथ</h2>
						<p align="justify">
							 <?php echo $row['description']; ?>
						</p>
							<h2 class="mt-20">नवनाथच्या काही पवित्र स्थळे</h2>
							<ul class="course_features_point" style="width: 99%">
								<li><i class="fa fa-hand-o-right"></i>  कानिफनाथ: माही, तालुका: पाथर्डी, जिल्हा: अहमदनगर (महाराष्ट्र) </li>
							</ul>
					</div>
				</div>
			</div>
		</div>
		
	<?php
		}
	}
	?>


 		

	</div>
</section>		
<?= $this->endSection() ?>
