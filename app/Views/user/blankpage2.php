<?= $this->extend('user/index') ?>

<?= $this->Section('nath_sampraday') ?>

<!-- Intro Section -->
			<section class="inner-intro  padding ptb-xs-40 bg-img1 overlay-dark light-color">
				<div class="container">
					<div class="row title">
						<h1>Blank Page</h1>
						<div class="page-breadcrumb">
							<a>index</a>/ <span>Blank Page</span>
						</div>
					</div>
				</div>
			</section>
			<!-- End Intro Section -->
			<!-- End Intro Section -->
			<div class="padding pt-xs-40">
				<div class="container">
					<div class="row">
						<div class="col-sm-12 ">
							<div class="heading-box pb-10">
								<h2><span>Our</span> heading</h2>
							</div>
							<div class="text-content">
								<p>Aenean suscipit eget mi act fermentum phasellus vulputate turpis tincidunt. Aenean suscipit eget mi act fermentum phasellus vulputate turpis tincidunt.									Aenean suscipit eget mi act fermentum phasellus vulputate turpis tincidunt. Aenean suscipit eget mi act fermentum phasellus vulputate turpis tincidunt.</p>
							</div>
						</div>
					</div>
				</div>
			</div>

			<section class="course-section__block event-sid padding blog-post" style="padding-top: 0px;">
				<div class="container">
					<div class="row">
						<div class="col-sm-12">
							<div class="plan-content-box__white__bg">
								<div class="row mt-10">
									<div class="col-sm-4 pr-0">
										<div class="table__block">
											<div class="event__date-time dis__block text-center">
												<div class="event__time">
													<span>Time: <i class="fa fa-clock-o"></i> 10 am - 11 am</span></br>
													<span><i class="fa fa-map-marker"></i> VENICE, India</span>
												</div>
												<div class="event__date ">
													25
												</div>
												<div class="event__month">
													June
												</div>
											</div>
										</div>
										<div class="venice_map">
											<div id="map"></div>

										</div>
										<div class="book__now padding-20 ">
											<h3> Ticket Book</h3>
											<a href="#" class="btn-text width-100per">BooK Now </a>
										</div>
									</div>

									<div class="col-sm-8 right_border ">
										<div class="single_plan__block after__none">
											<div class="plan-content-box plan-content-box__wight__bg width-100per">
												<h2>Learning Quality Graphic Design 2018.</h2>
												<p>
													Bimply dummy text of the printing and typesetting istryrem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer.when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuriesp into electronic.simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.
												</p>
												<p>
													when an unknown printer took a galley of type and scrambled it to make a type specimen book.Bimply dummy text of the printing and typesetting istryrem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer.when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuriesp into electronic.simply dummy text of the printing and typesetting industry
												</P>

												<blockquote class="dark-bg">
													We're not leaving here without Buster, man. Leave no crash-test dummy behind nihil impedit quo minus id quod maxime placeat facere!
												</blockquote>

											</div>
										</div>
									</div>
								</div>
							</div>

						</div>

					</div>

				</div>
			</section>
			<!--End Contact-->
<?= $this->endSection() ?>
