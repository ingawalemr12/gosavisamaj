<?= $this->extend('user/index') ?>

<?= $this->Section('homePage') ?>

		<!--  Main Banner Start Here-->
			<!-- Intro Section -->
			<section class="hero hero-top">
				<!--rev slider start-->
				<div class="fullwidthbanner">
					<div class="tp-fullscreen-banner">
						<ul>
							<!-- SLIDE -->
							<li data-transition="fade" data-slotamount="5" data-masterspeed="1000" data-title="Slide 1">
								<!-- MAIN IMAGE -->
								<img src="<?php echo base_url() ?>/public/assets/images/slider/slide.jpg"  alt="darkblurbg"  data-bgfit="cover" data-bgposition="left top" data-bgrepeat="no-repeat">
								<!-- Transparent Background -->
								<div class="tp-caption dark-translucent-bg"
								data-x="center"
								data-y="bottom"
								data-speed="500"
								data-easing="easeOutQuad"
								data-start="0"></div>
								<div class="banner-text">
									<div class="caption sfb bold text-center"
									data-x="center"
									data-y="120"
									data-speed="900"
									data-start="800"
									data-easing="Sine.easeOut">
										<h2 class="font-s"> Welcome To <span class="text-color"> EduPonits</span></h2>
									</div>
									<div class="caption sfb bold  text-center "
									data-x="center"
									data-y="200"
									data-speed="900"
									data-start="1500"
									data-easing="Sine.easeOIn">
										<p class="paragraph-s">
											EduPoints is a international leader in teaching students to write effectively,
											<br>
											learn from each other and think for themselves.
										</p>
									</div>
									<div class="caption sfb lowercase-caption text-center xs-hidden"
									data-x="center"
									data-y="280"
									data-speed="1000"
									data-start="2000"
									data-easing="Sine.easeOut">
										<a class="btn-text" href="#">Read more</a>
										<a class="btn-text white-btn__text" href="#">PURCHASE NOW</a>
									</div>
								</div>
							</li>
							<!-- SLIDE -->
							<!-- SLIDE -->
							<li data-transition="fade" data-slotamount="5" data-masterspeed="1000" data-title="Slide 1">
								<!-- MAIN IMAGE -->
								<img src="<?php echo base_url() ?>/public/assets/images/slider/slide1.jpg"  alt="darkblurbg"  data-bgfit="cover" data-bgposition="left top" data-bgrepeat="no-repeat">
								<!-- Transparent Background -->
								<div class="tp-caption dark-translucent-bg"
								data-x="center"
								data-y="bottom"
								data-speed="500"
								data-easing="easeOutQuad"
								data-start="0"></div>
								<div class="banner-text">
									<div class="caption sfb bold text-center"
									data-x="center"
									data-y="120"
									data-speed="900"
									data-start="800"
									data-easing="Sine.easeOut">
										<h2 class="font-s"><span class="text-color"> नाथ </span> संप्रदाय </h2>
									</div>
									<div class="caption sfb bold  text-center "
									data-x="center"
									data-y="200"
									data-speed="900"
									data-start="1500"
									data-easing="Sine.easeOIn">
										<p class="paragraph-s">
										 गोरक्ष् जलंधर चार्पाटाच| अडबांग  कानिफ मच्छिंद्रध्याय |  
										 <br>चौरंगी रेवाणक भर्तीसँडन्या | भुम्या बाबुवर्नवनाथसिद्धा: | 
										</p>
									</div>
									<div class="caption sfb lowercase-caption text-center xs-hidden"
									data-x="center"
									data-y="280"
									data-speed="1000"
									data-start="2000"
									data-easing="Sine.easeOut">
										<a class="btn-text" href="#">Read more</a>
										<a class="btn-text white-btn__text" href="#">PURCHASE NOW</a>
									</div>
								</div>
							</li>
							<!-- SLIDE -->
							<!-- SLIDE -->
							<li data-transition="fade" data-slotamount="5" data-masterspeed="1000" data-title="Slide 1">
								<!-- MAIN IMAGE -->
								<img src="<?php echo base_url() ?>/public/assets/images/slider/slide2.jpg"  alt="darkblurbg"  data-bgfit="cover" data-bgposition="left top" data-bgrepeat="no-repeat">
								<!-- Transparent Background -->
								<div class="tp-caption dark-translucent-bg"
								data-x="center"
								data-y="bottom"
								data-speed="500"
								data-easing="easeOutQuad"
								data-start="0"></div>
								<div class="banner-text">
									<div class="caption sfb bold text-center"
									data-x="center"
									data-y="120"
									data-speed="900"
									data-start="800"
									data-easing="Sine.easeOut">
										<h2 class="font-s"> Our Best Study<span class="text-color"> Institute</span></h2>
									</div>
									<div class="caption sfb bold  text-center "
									data-x="center"
									data-y="200"
									data-speed="900"
									data-start="1500"
									data-easing="Sine.easeOIn">
										<p class="paragraph-s">
											Lorem ipsum dolor sit amet consecte tur adipiscing
											<br>
											titor diam, a accumsan justo laoreet
										</p>
									</div>
									<div class="caption sfb lowercase-caption text-center xs-hidden"
									data-x="center"
									data-y="280"
									data-speed="1000"
									data-start="2000"
									data-easing="Sine.easeOut">
										<a class="btn-text" href="#">Read more</a>
										<a class="btn-text white-btn__text" href="#">PURCHASE NOW</a>
									</div>
								</div>
							</li>
							<!-- SLIDE -->

						</ul>
					</div>
				</div>
				<!--full width banner-->

				<!--revolution end-->
			</section>
			<div class="clearfix"></div>
			<!-- End Intro Section -->

			<!-- Offer -->
			<section class="padding ptb-xs-40 overflow_hidden offer_section">

				<div class="container">
					<div class="row">
						<div class="col-sm-12">
							<div class="services__items">

								<div class="col-md-4 mb-xs-30 mb-sm-30">
									<div class="about-block zoom-img clearfix">
										<figure>
											<a href="#"><img class="img-responsive" src="<?php echo base_url() ?>/public/assets/images/gal7.jpg" alt="Photo"></a>
										</figure>
										<div class="text-box mt-25">
											<div class="box-title mb-15">
												<h3><a href="#">नाथपंथी डवरी गोसावी</a></h3>
											</div>
											<div class="text-content mb-20">
												<p>
													महाराष्ट्रातील भटक्या जमातीतील नाथपंथी डवरी गोसावी ही एक जमात आहे.भटक्या विमुक्त जमाती सामाजिक, आर्थिक सुरक्षेच्या शोधात भटकंती करणार्‍यांचा समुदाय आहे.
												</p>
											</div>
											<a href="<?php echo base_url('nath-panthi') ?>" class="btn-text"> Read More</a>
										</div>
									</div>
								</div>

								<div class="col-md-4 mb-xs-30 mb-sm-30">
									<div class="about-block zoom-img clearfix">
										<figure>
											<a href="#"><img class="img-responsive" src="<?php echo base_url() ?>/public/assets/images/gal8.jpg" alt="Photo"></a>
										</figure>
										<div class="text-box mt-25">
											<div class="box-title mb-15">
												<h3><a href="#">Student Life</a></h3>
											</div>
											<div class="text-content mb-20">
												<p>
													It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.
												</p>

											</div>
											<a href="#" class="btn-text"> Read More</a>
										</div>
									</div>
								</div>

								<div class="col-md-4">
									<div class="about-block zoom-img clearfix">
										<figure>
											<a href="#"><img class="img-responsive" src="<?php echo base_url() ?>/public/assets/images/gal9.jpg" alt="Photo"></a>
										</figure>
										<div class="text-box mt-25">
											<div class="box-title mb-15">
												<h3><a href="#">College Campus</a></h3>
											</div>
											<div class="text-content mb-20">
												<p>
													It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.
												</p>

											</div>
											<a href="#" class="btn-text"> Read More</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

			</section>
			<!-- Offer End-->

			<section id="choose_us" class="padding ptb-xs-40 overflow_hidden secondary_bg dark-color">
				<div class="image_cover image_cover_right"></div>
				<!-- half image background element -->
				<div class="container">
					<div class="row">
						<div class="col-sm-6">
							<div class="creative_heading  pb-20">
								<h2>|| सिध्दयोगी समनाथ महाराज ||</h2>
								<p> <strong>हिंदू - नाथपंथी ( डवरी ) गोसावी समाजाचे संस्थापक</strong></p>
							</div>
							<div class="text">
								<p align="justify">
								<strong>|| सिध्दयोगी समनाथ महाराज ||</strong>
								 श्री योगी समनाथ महाराज यांनी हिंदू नाथपंती डवरी गोसावी समाजाची स्थापना पंधराव्या शतकात केली होती. जरी धर्माची नावे आणि धर्म संस्थापक वेगवेगळे असले तरी त्या धर्माच्या स्थापने मागिल कारणे व समाजीक बांधिलकी एकच होती. एखाद्या धर्माची स्थापना करणे व त्याची व्यवस्था परंपरा निर्माण करणे हेच त्या धर्म गुरूने त्या धर्माला दिलेले योगदान असते.
								</p>
								<p align="justify">
								 नाथ संप्रदाय वर्णव्यवस्थेला मानत नाही. भारतातील वेगवेगळ्या धर्मपंथांशी नाथपंथाचा कोणत्या तरी स्वरूपात संबंध आलेला दिसतो.</p>
							</div>
							<div class="wprt-list col-md-6 mb-20">
								<span class="icon"><i class="fa fa-dot-circle-o" aria-hidden="true"></i></span>
								<span>नाथ संप्रदाय हा भारतातील एक शैव संप्रदाय आहे. </span>
							</div>

							<div class="wprt-list col-md-6 mb-20">
								<span class="icon"><i class="fa fa-dot-circle-o" aria-hidden="true"></i></span>
								<span>"नाथ" या संज्ञेचा अर्थ रक्षणकर्ता / स्वामी असा होतो. </span>
							</div>

							<div class="wprt-list col-md-6 mb-20">
								<span class="icon"><i class="fa fa-dot-circle-o" aria-hidden="true"></i></span>
								<span>नाथ संप्रदायाची स्थापना मत्स्येंद्रनाथ ऊर्फ मच्छिंद्रनाथ यांनी केली. </span>
							</div>

							<div class="wprt-list col-md-6 mb-40">
								<span class="icon"><i class="fa fa-dot-circle-o" aria-hidden="true"></i></span>
								<span>शिव यांच्यापासून या संप्रदायाचा उगम झाला म्हणून त्याला "नाथ संप्रदाय" नाव मिळाले.  </span>
							</div>

							<div class="but-read">
								<a href="#" class="btn-text"> Read More</a>
							</div>

						</div>
					

					</div>
				</div>
			</section>

			<!--Team Section-->
			<section id="team" class="padding ptb-xs-40">
				<div class="container">
					<div class="row pb-30 text-center">
						<div class="col-sm-6 col-sm-offset-3">
							<div class="creative_heading">
								<h2>Learn From <span>The Best</span></h2>
							</div>
							<p>
								Lorem ipsum dolor sit amet, consectetur adipiscing elit. Tria genera bonorum; Poterat autem inpune; Quod cum dixissent, ille contra. Duo Reges:
							</p>
						</div>
					</div>
					<div class="row text-center">
						<div class="col-md-3 col-sm-6 mb-xs-30 mb-sm-30 ">
							<div class="box-hover img-scale">
								<figure>
									<img src="<?php echo base_url() ?>/public/assets/images/team/team1.jpg" alt="">
								</figure>
								<div class="team-block">
									<strong>Shahriyar Ahmed</strong><span>Head Teacher</span>
									<hr class="small-divider border-white">
									<ul class="social-icons">
										<li>
											<a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
										</li>
										<li>
											<a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
										</li>
										<li>
											<a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a>
										</li>
										<li>
											<a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
										</li>
										<li>
											<a href="#"><i class="fa fa-skype" aria-hidden="true"></i></a>
										</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="col-md-3 col-sm-6 mb-xs-30 mb-sm-30">
							<div class="box-hover img-scale">
								<figure>
									<img src="<?php echo base_url() ?>/public/assets/images/team/team2.jpg" alt="">
								</figure>
								<div class="team-block">
									<strong>Keira Knightley</strong><span>Vice Head Teacher</span>
									<hr class="small-divider border-white">
									<ul class="social-icons">
										<li>
											<a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
										</li>
										<li>
											<a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
										</li>
										<li>
											<a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a>
										</li>
										<li>
											<a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
										</li>
										<li>
											<a href="#"><i class="fa fa-skype" aria-hidden="true"></i></a>
										</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="col-md-3 col-sm-6 mb-xs-30 ">
							<div class="box-hover img-scale">
								<figure>
									<img src="<?php echo base_url() ?>/public/assets/images/team/team3.jpg" alt="">
								</figure>
								<div class="team-block">
									<strong>Jennifer Lawrence</strong><span>Advisory Professor</span>
									<hr class="small-divider border-white">
									<ul class="social-icons">
										<li>
											<a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
										</li>
										<li>
											<a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
										</li>
										<li>
											<a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a>
										</li>
										<li>
											<a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
										</li>
										<li>
											<a href="#"><i class="fa fa-skype" aria-hidden="true"></i></a>
										</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="col-md-3 col-sm-6">
							<div class="box-hover img-scale">
								<figure>
									<img src="<?php echo base_url() ?>/public/assets/images/team/team4.jpg" alt="">
								</figure>
								<div class="team-block">
									<strong>Asif Al Islam</strong><span>Design Teacher</span>
									<hr class="small-divider border-white">
									<ul class="social-icons">
										<li>
											<a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
										</li>
										<li>
											<a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
										</li>
										<li>
											<a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a>
										</li>
										<li>
											<a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
										</li>
										<li>
											<a href="#"><i class="fa fa-skype" aria-hidden="true"></i></a>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<!--Team Section End-->

			<!-- Project Section -->
			<div id="project-section" class="dark-bg clearfix padding ptb-xs-40">

				<div class="width-25per dark-bg  fl">
					<div class=" plr-30 m-height ">
						<div class="creative_heading mt-40 mb-20">
							<h2>Latest <span>Courses</span></h2>
						</div>
						<div class="block-content">
							
							<p class="mtb-30">
								Lorem ipsum dolor sit amet, consectetur adipiscing elit. Tria genera bonorum; Poterat autem inpune; Quod cum dixissent, ille contra. Duo Reges: constructio interrete.
							</p>

						</div>
					</div>
				</div>
				<div class="fl grid tab-respons">
					<div id="project">

						<div class="item">
							<div class="clearfix bg-area">
								<div class="image-box">
									<img class="img-responsive" src="<?php echo base_url() ?>/public/assets/images/project/project-img1.jpg" alt="Photo">

								</div>
								<div class="content-box">

									<div class="donate-price">
										Course Fee: &nbsp; <b>$12,210 </b>
									</div>
									<h4>Philosopphy</h4>
									<p>
										Lorum ipsum iure reprhe pu nderit sit amet,consectetur neque.
									</p>
									<div class="link">
										<a href="#" class="btn-text">Read More</a>
									</div>
								</div>
							</div>

						</div>

						<div class="item">
							<div class="clearfix bg-area">
								<div class="image-box">
									<img class="img-responsive" src="<?php echo base_url() ?>/public/assets/images/project/project-img2.jpg" alt="Photo">

								</div>
								<div class="content-box">

									<div class="donate-price">
										Course Fee: &nbsp; <b>$12,210 </b>
									</div>
									<h4>IELTS</h4>
									<p>
										Lorum ipsum iure reprhe pu nderit sit amet,consectetur neque.
									</p>
									<div class="link">
										<a href="#" class="btn-text">Read More</a>
									</div>
								</div>
							</div>

						</div>

						<div class="item">
							<div class="clearfix bg-area">
								<div class="image-box">
									<img class="img-responsive" src="<?php echo base_url() ?>/public/assets/images/project/project-img3.jpg" alt="Photo">

								</div>
								<div class="content-box">

									<div class="donate-price">
										Course Fee: &nbsp; <b>$12,210 </b>
									</div>
									<h4>Regular MBA</h4>
									<p>
										Lorum ipsum iure reprhe pu nderit sit amet,consectetur neque.
									</p>
									<div class="link">
										<a href="#" class="btn-text">Read More</a>
									</div>
								</div>
							</div>

						</div>

						<div class="item">
							<div class="clearfix bg-area">
								<div class="image-box">
									<img class="img-responsive" src="<?php echo base_url() ?>/public/assets/images/project/img4.jpg" alt="Photo">

								</div>
								<div class="content-box">

									<div class="donate-price">
										Course Fee: &nbsp; <b>$12,210 </b>
									</div>
									<h4>GMAT</h4>
									<p>
										Lorum ipsum iure reprhe pu nderit sit amet,consectetur neque.
									</p>
									<div class="link">
										<a href="#" class="btn-text">Read More</a>
									</div>
								</div>
							</div>

						</div>

						<div class="item">
							<div class="clearfix bg-area">
								<div class="image-box">
									<img class="img-responsive" src="<?php echo base_url() ?>/public/assets/images/project/project-img1.jpg" alt="Photo">

								</div>
								<div class="content-box">

									<div class="donate-price">
										Course Fee: &nbsp; <b>$12,210 </b>
									</div>
									<h4>Philosopphy</h4>
									<p>
										Lorum ipsum iure reprhe pu nderit sit amet,consectetur neque.
									</p>
									<div class="link">
										<a href="#" class="btn-text">Read More</a>
									</div>
								</div>
							</div>

						</div>

						<div class="item">
							<div class="clearfix bg-area">
								<div class="image-box">
									<img class="img-responsive" src="<?php echo base_url() ?>/public/assets/images/project/project-img2.jpg" alt="Photo">

								</div>
								<div class="content-box">

									<div class="donate-price">
										Course Fee: &nbsp; <b>$12,210 </b>
									</div>
									<h4>IELTS</h4>
									<p>
										Lorum ipsum iure reprhe pu nderit sit amet,consectetur neque.
									</p>
									<div class="link">
										<a href="#" class="btn-text">Read More</a>
									</div>
								</div>
							</div>

						</div>

						<div class="item">
							<div class="clearfix bg-area">
								<div class="image-box">
									<img class="img-responsive" src="<?php echo base_url() ?>/public/assets/images/project/project-img3.jpg" alt="Photo">

								</div>
								<div class="content-box">

									<div class="donate-price">
										Course Fee: &nbsp; <b>$12,210 </b>
									</div>
									<h4>Regular MBA</h4>
									<p>
										Lorum ipsum iure reprhe pu nderit sit amet,consectetur neque.
									</p>
									<div class="link">
										<a href="#" class="btn-text">Read More</a>
									</div>
								</div>
							</div>

						</div>

						<div class="item">
							<div class="clearfix bg-area">
								<div class="image-box">
									<img class="img-responsive" src="<?php echo base_url() ?>/public/assets/images/project/img4.jpg" alt="Photo">

								</div>
								<div class="content-box">

									<div class="donate-price">
										Course Fee: &nbsp; <b>$12,210 </b>
									</div>
									<h4>GMAT</h4>
									<p>
										Lorum ipsum iure reprhe pu nderit sit amet,consectetur neque.
									</p>
									<div class="link">
										<a href="#" class="btn-text">Read More</a>
									</div>
								</div>
							</div>

						</div>

					</div>
				</div>

			</div>
			<!-- Project Section -->

			<!-- Testimonial -->
			<section class="testimonial-section padding ptb-xs-40 gray-bg">
				<div class="container">

					<div class="row pb-30 text-center dark-color">
						<div class="col-sm-6 col-sm-offset-3">
							<div class="creative_heading">
								<h2><span> What</span> People Say</h2>
							</div>
							<p>
								Lorem ipsum dolor sit amet, consectetur adipiscing elit. Tria genera bonorum; Poterat autem inpune; Quod cum dixissent, ille contra. Duo Reges:
							</p>
						</div>
					</div>
					<div class="row">
						<div class="carousel-slider carousel-box nf-carousel-theme arrow_theme">
							<div class="carousel-item col-sm-4 ">
								<div class="testimonial-block">
									<figure class="testimonial-img">
										<img class="img-circle img-border" src="<?php echo base_url() ?>/public/assets/images/testimonial/1.jpg" alt="">
									</figure>
									<h3 class="testimonial-author">Selly Thomas</h3>
									<hr class="small-divider">
									<p>
										At vero eos et accusamus et iusto odio dignissimos ducimus qui.
									</p>
									<span class="star"> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> </span>
								</div>
							</div>

							<div class="carousel-item col-sm-4 ">
								<div class="testimonial-block">
									<figure class="testimonial-img">
										<img class="img-circle img-border" src="<?php echo base_url() ?>/public/assets/images/testimonial/2.jpg" alt="">
									</figure>
									<h3 class="testimonial-author writer">Jane Anselmo</h3>
									<hr class="small-divider">
									<p>
										At vero eos et accusamus et iusto odio dignissimos ducimus qui.
									</p>
									<span class="star"> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> </span>
								</div>
							</div>

							<div class="carousel-item col-sm-4 ">
								<div class="testimonial-block">
									<figure class="testimonial-img">
										<img class="img-circle img-border" src="<?php echo base_url() ?>/public/assets/images/testimonial/3.jpg" alt="">
									</figure>
									<h3 class="testimonial-author">Melisa Barry</h3>
									<hr class="small-divider">
									<p>
										At vero eos et accusamus et iusto odio dignissimos ducimus qui.
									</p>
									<span class="star"> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> </span>
								</div>
							</div>

						</div>
					</div>
				</div>
			</section>
			<!-- Testimonial -->

			<!-- Client Logos Section -->
			<section id="client-logos" class="padding ptb-xs-40 wow fadeIn ptb ">
				<div class="container">
					<div class="row pb-30 text-center">
						<div class="col-sm-6 col-sm-offset-3">
							<div class="creative_heading">
								<h2>Our <span>Partners</span></h2>
							</div>
							<p>
								Lorem ipsum dolor sit amet, consectetur adipiscing elit. Tria genera bonorum; Poterat autem inpune; Quod cum dixissent, ille contra. Duo Reges:
							</p>
						</div>
					</div>

					<div class="owl-carousel client-carousel nf-carousel-theme ">
						<div class="item">
							<div class="client-logo">
								<img src="<?php echo base_url() ?>/public/assets/images/client-logo/1.png" alt="" />
							</div>
						</div>
						<div class="item">
							<div class="client-logo">
								<img src="<?php echo base_url() ?>/public/assets/images/client-logo/2.png" alt="" />
							</div>
						</div>
						<div class="item">
							<div class="client-logo">
								<img src="<?php echo base_url() ?>/public/assets/images/client-logo/3.png" alt="" />
							</div>
						</div>
						<div class="item">
							<div class="client-logo">
								<img src="<?php echo base_url() ?>/public/assets/images/client-logo/4.png" alt="" />
							</div>
						</div>
						<div class="item">
							<div class="client-logo">
								<img src="<?php echo base_url() ?>/public/assets/images/client-logo/5.png" alt="" />
							</div>
						</div>

						<div class="item">
							<div class="client-logo">
								<img src="<?php echo base_url() ?>/public/assets/images/client-logo/1.png" alt="" />
							</div>
						</div>
						<div class="item">
							<div class="client-logo">
								<img src="<?php echo base_url() ?>/public/assets/images/client-logo/2.png" alt="" />
							</div>
						</div>
						<div class="item">
							<div class="client-logo">
								<img src="<?php echo base_url() ?>/public/assets/images/client-logo/3.png" alt="" />
							</div>
						</div>
						<div class="item">
							<div class="client-logo">
								<img src="<?php echo base_url() ?>/public/assets/images/client-logo/4.png" alt="" />
							</div>
						</div>
						<div class="item">
							<div class="client-logo">
								<img src="<?php echo base_url() ?>/public/assets/images/client-logo/5.png" alt="" />
							</div>
						</div>

					</div>
				</div>
			</section>
			<!-- End Client Logos Section -->
<?= $this->endSection() ?>
