<?= $this->extend('admin/dashboard') ?>

<?= $this->Section('blog_view') ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Blog</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Blogs</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-12">
          	
            <div class="card">
              
              	<div class="card-header">
	              	<div class="card-title">
	                  <form id="searchFrm" name="searchFrm" method="get" action="">
	                    <div class="input-group mb-0">
	                        <input type="text" name="q" value="" 
	                        class="form-control" placeholder="search">
	                          <div class="input-group-append">
	                              <button class="input-group-text" id="basic-addonl"> 
	                                <i class="fas fa-search"></i>
	                              </button>
	                         </div>
	                    </div>
	                  </form>
	                </div>
	              	<div class="card-tools">
	              		<a href="<?php echo base_url('admin/blog') ?>" class="btn btn-primary">
	              			<i class="fas fa-plus"></i> Create
	              		</a>
	             	</div>
              	</div>
              	<div class="card-body">
              		<table class="table">
              			<tr>
              				<th >#</th>
                      <th >Full Name</th>
	                    <th>Title</th>
    	                <th >Mobile Number</th>
    	                <th >File</th>
    	                <th>Description</th>
    	                <th >Staus</th>
        	            <th >Created_at</th>
          					  <th class="text-center">Action</th>
              			</tr>
                    
                    <tr>
                      <td>   </td>
                     	<td>   </td>
                     	<td>   </td>
	                    <td>   </td>
	                    <td>   </td>
                    	<td>   </td>
                      <td>   </td>
	                    <td>   </td>
	                    <td> 
	                     	<a href="#" class="btn btn-sm btn-primary">
                      		<i class="far fa-edit"></i>
                      	</a>
                      	<a href="#" class="btn btn-sm btn-danger">
                      		<i class="far fa-trash-alt"></i>
                       	</a>
                      </td>
                    </tr>                      
                    <tr>
                      <td colspan="7">Record not found</td>
                    </tr>
                  </table>
                </div>
            </div>
         </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?= $this->endSection() ?>