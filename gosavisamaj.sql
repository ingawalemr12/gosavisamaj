-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: May 16, 2021 at 06:32 AM
-- Server version: 10.4.17-MariaDB
-- PHP Version: 7.4.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `gosavisamaj`
--

-- --------------------------------------------------------

--
-- Table structure for table `photos_details`
--

CREATE TABLE `photos_details` (
  `id` int(9) NOT NULL,
  `mobile` varchar(100) NOT NULL,
  `name` text NOT NULL,
  `city` varchar(100) NOT NULL,
  `image` varbinary(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `photos_details`
--

INSERT INTO `photos_details` (`id`, `mobile`, `name`, `city`, `image`, `description`, `created_at`) VALUES
(3, '+91 9970410333', 'mahadev ingawale', 'Shirwal', 0x313632303938363136365f66373066303462333930663735656361386331352e6a7067, '<p>mobilemobilemobile<br></p>', '2021-05-14 15:26:06'),
(4, '+91 9970410333', 'mahadev ingawale', 'Pune', 0x313632313031313239375f62343837396262346332623637663239383634322e6a7067, '<p><strong style=\"font-weight: bold; color: rgb(50, 50, 50); font-family: Roboto, sans-serif; font-size: 15px; text-align: justify;\">महाराष्ट्रातील भटक्या जमातीतील नाथपंथी डवरी गोसावी ही एक जमात आहे.</strong><span style=\"color: rgb(50, 50, 50); font-famil', '2021-05-14 22:24:57'),
(10, '9970410122', 'demo category', 'Satara', 0x313632313031333831325f38313934623232633732353935306162633630392e6a7067, '<p>in codeigniter 4 data is showing that record is inserted, but it not inserting in mysql database table<br></p>', '2021-05-14 23:06:52');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `photos_details`
--
ALTER TABLE `photos_details`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `photos_details`
--
ALTER TABLE `photos_details`
  MODIFY `id` int(9) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
